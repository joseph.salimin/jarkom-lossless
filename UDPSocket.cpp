#include "UDPSocket.hpp"

void UDPSocket::setNetSocket() {
    this->net_socket = socket(AF_INET, SOCK_DGRAM, 0);
    if (this->net_socket == -1) {
        handleError("ERROR Opening Socket");
    }
}
