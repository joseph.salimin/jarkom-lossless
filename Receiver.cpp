//
// Created by joseph on 10/27/18.
//

#include "Receiver.hpp"

Receiver::Receiver(std::string file, uint32_t wsize, uint32_t bsize, in_port_t port) {
    this->writer_handler.output_stream = fopen(file.c_str(), "wb");
    if (this->writer_handler.output_stream == nullptr) handleError("Failed to open file");
    this->writer_handler.buffer_length = 0;
    this->rws = wsize;
    this->lfr = 0;
    this->laf = this->lfr + this->rws;
    this->buffer_size = bsize;
    this->setSocket(port);
}

void Receiver::setSocket(in_port_t port) {
    this->setNetSocket();
    this->setSocketTimeout();
    this->setServerAddress(port);
    this->setBinding();
}

void Receiver::setSocketTimeout() {
    struct timeval tv{};
    tv.tv_usec = 0;
    tv.tv_sec = RECEIVE_TIMEOUT;
    if (setsockopt(this->net_socket, SOL_SOCKET, SO_RCVTIMEO, (const char*) &tv, sizeof(tv)) < 0) {
        handleError("Error Setting Socket Timeout");
    }
}

void Receiver::setServerAddress(in_port_t port) {
    // Empty the address of address first!
    memset((char *) &(this->address), 0, sizeof(this->address));
    this->address.sin_family = AF_INET;
    this->address.sin_port = htons(port);
    this->address.sin_addr.s_addr = htonl(INADDR_ANY);
}

void Receiver::setBinding() {
    this->binding = bind(this->net_socket, (struct sockaddr*) &(this->address), sizeof(this->address));
    if (this->binding == -1) {
        handleError("ERROR Binding Socket");
    }
}

bool Receiver::checkPacketWithinRWS(Packet &p) {
    return p.sequence_number > this->lfr && p.sequence_number <= this->laf;
}

bool Receiver::checkBeforeLFR(Packet &p) {
    return p.sequence_number <= this->lfr;
}

void Receiver::sendNAK() {
    ACK ack = createACK(true, this->lfr);
    char * buff_ack = serializeACK(ack);
    if (sendto(this->net_socket, buff_ack, sizeof(buff_ack), 0,
               (struct sockaddr *) &this->client_addr, sizeof(this->client_addr)) >= 0) {
        printf("Send NAK %d\n", this->lfr + 1);
    }
    delete[] buff_ack;
}

void Receiver::sendACK() {
    ACK ack = createACK(false, this->lfr);
    char * buff_ack = serializeACK(ack);
    if (sendto(this->net_socket, buff_ack, 6, 0,
               (struct sockaddr *) &this->client_addr, sizeof(this->client_addr)) >= 0) {
        printf("Send ACK %d\n", this->lfr + 1);
    }
    delete[] buff_ack;
}

void Receiver::addToPackets(Packet& p) {
    uint32_t index = p.sequence_number % this->rws;
    memset(&(this->packets[index]), 0, sizeof(Packet));
    this->packets[index] = p;
}

void Receiver::addToFileBuffer(Packet& p) {
    if (this->writer_handler.buffer_length + p.data_length > this->buffer_size * MAX_DATA_LENGTH) {
        fwrite(this->writer_handler.buffer_stream, 1, this->writer_handler.buffer_length, this->writer_handler.output_stream);
        memset(this->writer_handler.buffer_stream, 0, (buffer_size * MAX_DATA_LENGTH));
        this->writer_handler.buffer_length = 0;
    }
    memcpy(this->writer_handler.buffer_stream + this->writer_handler.buffer_length, p.data, p.data_length);
    this->writer_handler.buffer_length += p.data_length;
}

bool Receiver::checkSendACK() {
    for (int i = this->lfr + 2; i <= this->laf; i++) {
        Packet p = this->packets[i % this->rws];
        if (p.sequence_number == i) return false;
    }
    return true;
}

void Receiver::handlePacket(char* buffer, size_t buf_len) {
    Packet p{};
    if (unserializePacket(buffer, buf_len, p)) {
        if (this->checkPacketWithinRWS(p)) {
            if (p.SOH == EOT) {
                this->end = true;
                return;
            }
            this->addToPackets(p);
            while (this->lfr == p.sequence_number - 1) {
                this->lfr = p.sequence_number;
                this->laf = this->lfr + this->rws;
                this->addToFileBuffer(p);
                p = this->packets[(this->lfr + 1) % this->rws];
            }
            std::cout << "Get packet " << p.sequence_number << std::endl;
            if (checkSendACK()) this->sendACK();
            else this->sendNAK();
        } else if (this->checkBeforeLFR(p)) {
            if (checkSendACK()) this->sendACK();
            else this->sendNAK();
        }
    } else {
        this->sendNAK();
    }
}

void Receiver::run() {
    this->writer_handler.buffer_stream = new char[this->buffer_size * MAX_DATA_LENGTH];
    memset(this->writer_handler.buffer_stream, 0, (buffer_size * MAX_DATA_LENGTH));
    this->packets = new Packet[this->rws];
    memset(this->packets, 0, (sizeof(Packet) * this->rws));
    // Time to running the algorithm!!
    socklen_t addr_len = sizeof(this->client_addr);
    char buffer_socket[PACKET_LENGTH + 1];
    while (!this->end) {
        ssize_t buf_len = recvfrom(this->net_socket, buffer_socket, sizeof(buffer_socket), 0,
                                   (struct sockaddr *) &this->client_addr, &addr_len);
        if (buf_len >= 0) this->handlePacket(buffer_socket, (size_t) buf_len);
    }
    fwrite(this->writer_handler.buffer_stream, 1, this->writer_handler.buffer_length, this->writer_handler.output_stream);
    fclose(this->writer_handler.output_stream);
}