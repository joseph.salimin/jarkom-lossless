//
// Created by joseph on 10/29/18.
//

#ifndef _UTILS_HPP
#define _UTILS_HPP

#include <string>
#include <iostream>
#include <fstream>

void handleError(char const *msg);
char* readFileContent(std::ifstream& is, size_t buf_size);

#endif //_UTILS_HPP
