//
// Created by joseph on 10/27/18.
//

#ifndef _SENDER_H
#define _SENDER_H

#include <sys/stat.h>
#include "UDPSocket.hpp"

typedef struct {
    FILE* reader_stream;
    uint32_t total_packets;
    uint32_t packets_moved;
    uint32_t packets_in_buffer;
    struct stat st;
    char buffer[1024];
} FileReader;

class Sender: public UDPSocket {
private:
    // Variables
    uint32_t sws; // Sending Windows Size
    uint32_t lar; // Last Acknowledgement Received
    uint32_t lfs; // Last Frame Sent
    int connection;  // Connection
    PacketSender* packets;
    FileReader reader_handler;
    // Methods
    void initReaderHandler(const char*);
    void setReceiverAddr(std::string ip, in_port_t port);
    void setSocket(in_port_t, std::string&);
    void setSocketTimeout();
    void readFileToBuffer();
public:
    Sender(std::string &file, uint32_t wsize, uint32_t bsize, std::string &ip, in_port_t port);
    void createConnection();
    void run() override;
};

#endif //_SENDER_H
