//
// Created by joseph on 10/27/18.
//

#include <cstring>
#include <cstdio>
#include "Packet.hpp"

Packet createPacket(uint32_t sequence_number, const char data[], size_t length) {
    Packet p;
    memset(&(p.data[0]), '\0', sizeof(p.data));
    p.SOH = SOH_NUMBER;
    p.sequence_number = sequence_number;
    p.data_length = length;
    memcpy(p.data, data, p.data_length);
    p.checksum = ~calculatePacketCheckSum(p);
    return p;
}

Packet createEndPacket(uint32_t sequence_number) {
    Packet p;
    memset(&(p.data[0]), '\0', sizeof(p.data));
    p.SOH = EOT;
    p.sequence_number = sequence_number;
    p.data_length = 1;
    memset(p.data, 0, 1);
    p.checksum = ~calculatePacketCheckSum(p);
    return p;
}

ACK createACK(bool negative, uint32_t sequence_number) {
    ACK ack;
    if (negative) ack.ACK = NAK_NUMBER;
    else ack.ACK = ACK_NUMBER;
    ack.next_sequence_number = sequence_number + 1;
    ack.checksum = ~ calculateACKCheckSum(ack);
    return ack;
}

bool unserializePacket(char * buffer, size_t length, Packet& p) {
    p.sequence_number = 0;
    p.data_length = 0;
    if (buffer[PACKET_OFFSET_SOH] == SOH_NUMBER) p.SOH = SOH_NUMBER;
    else if (buffer[PACKET_OFFSET_SOH] == EOT) p.SOH = EOT;
    else return false;
    for (size_t i = PACKET_OFFSET_SEQUENCE; i < PACKET_OFFSET_DATA_LENGTH; i++) {
        p.sequence_number += ((uint8_t) buffer[i]) << ((3 - i + PACKET_OFFSET_SEQUENCE) * 8);
    }
    for (size_t i = PACKET_OFFSET_DATA_LENGTH; i < PACKET_OFFSET_DATA; i++) {
        p.data_length += ((uint8_t) buffer[i]) << ((3 - i + PACKET_OFFSET_DATA_LENGTH) * 8);
    }
    size_t data_size = length - (PACKET_LENGTH - MAX_DATA_LENGTH);
    for (size_t i = PACKET_OFFSET_DATA; i < PACKET_OFFSET_DATA + data_size; i++) {
        p.data[i - PACKET_OFFSET_DATA] = buffer[i];
    }
    p.checksum = (uint8_t) buffer[length + PACKET_OFFSET_CHECKSUM];
    return verifyPacketCheckSum(p);
}

bool unserializeACK(char* buffer, size_t length, ACK& ack) {
    ack.ACK = (uint8_t) buffer[ACK_OFFSET_ACK];
    ack.next_sequence_number = 0;
    for (size_t i = ACK_OFFSET_SEQUENCE; i < ACK_OFFSET_CHECKSUM; i++) {
        ack.next_sequence_number += ((uint8_t) buffer[i]) << ((3 - i + ACK_OFFSET_SEQUENCE) * 8);
    }
    ack.checksum = (uint8_t) buffer[ACK_OFFSET_CHECKSUM];
    return verifyACKCheckSum(ack);
}

char * serializePacket(Packet &p, size_t length) {
    int bufferOffset = 0;
    char* buffer = new char[length];
    buffer[bufferOffset++] = p.SOH;
    for (char i = 0; i < 4; i++) {
        buffer[bufferOffset++] = (char) (p.sequence_number >> ((3 - i) * 8));
    }
    for (char i = 0; i < 4; i++) {
        char value = (char) (p.data_length >> ((3 - i) * 8));
        buffer[bufferOffset++] = value;
    }
    for (uint32_t i = 0; i < p.data_length; i++) {
        char value = p.data[i];
        buffer[bufferOffset++] = value;
    }
    buffer[bufferOffset] = (char) p.checksum;
    return buffer;
}

char * serializeACK(ACK &ack){
    int bufferOffset = 0;
    char * buffer = new char[ACK_LENGTH];
    buffer[bufferOffset++] = ack.ACK;
    for (char i = 0; i < 4; i++) {
        buffer[bufferOffset++] = (char) (ack.next_sequence_number >> ((3 - i) * 8));
    }
    buffer[ACK_OFFSET_CHECKSUM] = (char) ack.checksum;
    return buffer;
}

uint8_t calculatePacketCheckSum(Packet &p) {
    uint32_t output = p.SOH;
    char length = sizeof(uint32_t)/sizeof(uint8_t);
    for (size_t i = 0; i < length; i++) {
        output += (uint8_t) (p.sequence_number >> (i * 8));
    }
    for (size_t i = 0; i < length; i++) {
        output += (uint8_t) (p.data_length >> (i * 8));
    }
    output += output >> 8;
    for (size_t i = 0; i < p.data_length; i++) {
        output += p.data[i];
        output += output >> 8;
    }
    return (uint8_t) output;
}

uint8_t calculateACKCheckSum(ACK &ack) {
    uint32_t output = ack.ACK;
    char length = sizeof(uint32_t)/sizeof(uint8_t);
    for (size_t i = 0; i < length; i++) {
        output += (uint8_t) (ack.next_sequence_number >> (i * 8));
    }
    output += output >> 8;
    return (uint8_t) output;
}

bool verifyPacketCheckSum(Packet &p) {
    return (p.checksum + calculatePacketCheckSum(p)) == 0xFF;
}

bool verifyACKCheckSum(ACK &ack) {
    return (ack.checksum + calculateACKCheckSum(ack)) == 0xFF;
}